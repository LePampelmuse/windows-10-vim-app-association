# Windows 10 vim app association 
This vim.bat file allows you to set vim (installed in Linux-Subsystem) as the default app to open a certain type of file.

## Preparation/requirements
Install
- Linux-Subsystem
- Vim

## Install
Download/Clone vim.bat and move it to a directory of your choice.
Just right click your file (the filetype you want to associate with vim), click on "Open with", "more Apps", "Look for another App on this PC",
Choose vim.bat as default program and don't forget to click on "always use this app ..."


